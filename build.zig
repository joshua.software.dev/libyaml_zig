const std = @import("std");


// Although this function looks imperative, note that its job is to
// declaratively construct a build graph that will be executed by an external
// runner.
pub fn build(b: *std.Build) void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    // Standard optimization options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall. Here we do not
    // set a preferred release mode, allowing the user to decide how to optimize.
    const optimize = b.standardOptimizeOption(.{});

    const libyaml_dep = b.dependency("libyaml", .{ .target = target, .optimize = optimize });

    const libyaml = b.addStaticLibrary(.{
        .name = "libyaml",
        .optimize = optimize,
        .target = target,
    });

    libyaml.root_module.link_libc = true;
    libyaml.root_module.addCMacro("YAML_DECLARE_STATIC", "1");
    libyaml.root_module.addCMacro("YAML_VERSION_MAJOR", "0");
    libyaml.root_module.addCMacro("YAML_VERSION_MINOR", "2");
    libyaml.root_module.addCMacro("YAML_VERSION_PATCH", "5");
    libyaml.root_module.addCMacro("YAML_VERSION_STRING", "\"0.2.5\"");
    libyaml.addIncludePath(libyaml_dep.path("include/"));
    libyaml.addCSourceFiles(.{
        .files = &.{
            "src/api.c",
            "src/dumper.c",
            "src/emitter.c",
            "src/loader.c",
            "src/parser.c",
            "src/reader.c",
            "src/scanner.c",
            "src/writer.c",
        },
        .flags = &.{
            "-std=c89",
            "-fvisibility=hidden",
        },
        .root = libyaml_dep.path(""),
    });

    const libyaml_zig = b.addModule("libyaml_zig", .{
        .root_source_file = b.path("src/yaml.zig"),
    });
    libyaml_zig.addIncludePath(libyaml_dep.path("include/"));
    libyaml_zig.linkLibrary(libyaml);
}
