# libyaml-zig

This is a packaging of [nektro](https://github.com/nektro)'s [zig-yaml](https://github.com/nektro/zig-yaml) (not to be confused with [kubkon](https://github.com/kubkon)'s [zig-yaml](https://github.com/kubkon/zig-yaml/)) for the zig package manager. It provides: "A Yaml parser built on top of [libyaml](https://github.com/yaml/libyaml)".

# Branches
This is the master branch, it targets the latest zig stable version. At the time of writing, that means it targets compatibility with zig v0.12.0. Later may possibly work, but zig is an evolving language, so it also may not still be compatible.

If you need compatibility with zig's previous stable versions, see the [0.11.0](https://gitlab.com/joshua.software.dev/libyaml_zig/-/tree/0.11.0) branch.

# Installation

## In `build.zig.zon`

```zig
.{
    .name = "my_project",
    .version = "1.0.0",
    .dependencies = .{
        .libyaml_zig = .{
            // https://multiformats.io/multihash/#sha2-256-256-bits-aka-sha256
            // "1220" + sha256sum
            // to generate the expected hash needed here. run zig fetch "https://example.com/archive.tar.gz"
            .hash = "12200000000000000000000000000000000000000000000000000000000000000000",
            // Make sure to replace the "0"s below with the commit hash for the version you want to use.
            .url = "https://gitlab.com/joshua.software.dev/libyaml_zig/-/archive/0000000000000000000000000000000000000000/libyaml_zig-0000000000000000000000000000000000000000.tar.gz",
        },
    },
}

```

## In `build.zig`
```zig
const libyaml_zig_dep = b.dependency("libyaml_zig", .{
    .target = target,
    .optimize = optimize,
});

const my_static_lib = b.addStaticLibrary(.{
    .name = "my_static_lib",
    .root_source_file = .{ .path = "src/main.zig" },
    .target = target,
    .optimize = optimize,
});

my_static_lib.root_module.addImport("libyaml_zig", libyaml_zig_dep.module("libyaml_zig"));
```

# Usage

```zig
const libyaml_zig = @import("libyaml_zig");


fn parse(allocator: std.mem.Allocator, path: []const u8) !void {
    const file = std.fs.openFileAbsolute(path, .{});
    const file_text = try file.readToEndAlloc(allocator, std.math.maxInt(usize));
    // The parser does not clone relevant strings, instead providing pointers
    // into the input buffer. If this memory is freed before you are done using
    // your parsed objects, the memory it references will be invalid.
    defer allocator.free(file_text);

    var document = try libyaml_zig.parse(allocator, file_text);

    const my_parsed_field = document.mapping.get("my_field_key")
        orelse return error.MissingField;

    use_my_parsed_field(my_parsed_field);
}
```

Keeping the whole input buffer allocated so you can access a subset of its text is not ideal. Borrowing an idea from `std.json.Parsed` may be prudent here:

```zig
const std = @import("std");

const libyaml_zig = @import("libyaml_zig");


pub fn ParsedYaml(comptime T: type) type {
    return struct {
        arena: std.heap.ArenaAllocator,
        value: T,

        pub fn deinit(self: @This()) void {
            self.arena.deinit();
        }
    };
}

const Person = struct {
    name: []const u8,
};

fn parse_person(allocator: std.mem.Allocator, path: []const u8) !ParsedYaml(Person) {
    const file = std.fs.openFileAbsolute(path, .{});
    const file_text = try file.readToEndAlloc(allocator, std.math.maxInt(usize));
    defer allocator.free(file_text);

    var document = try libyaml_zig.parse(allocator, file_text);
    const name = document.mapping.get("name")
        orelse return error.MissingField;

    var person: ParsedYaml(Person) = .{
        .arena = std.heap.ArenaAllocator.init(allocator),
        .value = undefined,
    };
    person.value = .{
        .name = person.arena.allocator().dupe(u8, name),
    };

    // Now instead of needing the entire input buffer to remain allocated for
    // the parsed strings to remain valid, only the copies in the arena need to
    // be kept, and the input can be freed.
    return person;
}

pub fn main() !void {
    var gpa: std.heap.GeneralPurposeAllocator(.{}) = .{};
    defer _ = gpa.deinit();

    const person = try parse_person(gpa.allocator(), "/path/to/file.yaml");
    // don't forget to release your arena when you're done with it
    defer person.deinit();

    std.debug.print("Person: {s}\n", .{ person.value.name });
}
```